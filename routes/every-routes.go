package routes

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"olist-hack-bot/dao"
	"olist-hack-bot/shared"
	"slapfestas-site-api/messages"
)

func ReceiveMessageAI(w http.ResponseWriter, r *http.Request) {
	var sendMsg shared.SendMessage
	var datas map[string]interface{}

	body, err := ioutil.ReadAll(r.Body)
	messages.HandleErrors(err)
	json.Unmarshal(body, &sendMsg)

	/*replyKeyboardMarckup.Keyboard = [][]shared.KeyboardButton{
		[]shared.KeyboardButton{
			shared.KeyboardButton{
				Text: "Answer",
			},
			shared.KeyboardButton{
				Text: "Ask",
			},
		},
		[]shared.KeyboardButton{
			shared.KeyboardButton{
				Text: "Say",
			},
			shared.KeyboardButton{
				Text: "Speak",
			},
		},
	}*/
	sendMsg.ReplyMarkup.OneTimeKeyboard = true
	SendMessageTelegram(sendMsg, nil)
	fmt.Println(sendMsg)

	datas = make(map[string]interface{})

	datas["chat_id_telegram"] = sendMsg.ChatID
	datas["user_id_telegram"] = sendMsg.ChatID
	datas["chat_id_mercado_livre"] = sendMsg.IDMercadoLivre
	datas["token_mercado_livre"] = sendMsg.TokenMercadoLivre
	datas["product_id_mercado_livre"] = sendMsg.ProductIDMercadoLivre
	datas["product_nome_mercado_livre"] = sendMsg.ProductNomeMercadoLivre
	datas["question"] = sendMsg.Text
	datas["suggested_answer"] = sendMsg.ReplyMarkup.Keyboard[0][0].Text

	dao.SetTable("messages_input_output")
	dao.Save(datas)
}

func ReceiveMessageTelegram(w http.ResponseWriter, r *http.Request) {
	body := &shared.UpdateMessage{}
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		fmt.Println("could not decode request body", err)
		return
	}
	fmt.Println(body)
	// enviar para o mercado livre e para a IA
	// SendMessageAI(body)
	SendMessageML(body)

	UpdateAnswerSent(body.Msg.Text, "chat_id_telegram", body.Msg.RefChat.ID)
}

func SendMessageTelegram(sendMsg shared.SendMessage, formatSend map[string]interface{}) {
	var err error
	var reqBytes []byte

	if formatSend == nil {
		reqBytes, err = json.Marshal(sendMsg)
	} else {
		reqBytes, err = json.Marshal(formatSend)
	}
	shared.HandleErrors(err)

	res, err := http.Post(shared.TelegramApiUrl+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	shared.HandleErrors(err)

	if res.StatusCode != http.StatusOK {
		log.Println("unexpected status" + res.Status)
	}
}

func SendMessageAI(sendMsg *shared.UpdateMessage) {
	var err error
	var reqBytes []byte
	var datas []interface{}
	var messages shared.MessagesInputOutput

	dao.SetTable("messages_input_output")

	datas = append(datas, sendMsg.Msg.RefChat.ID)
	dao.SetFilterValues(datas)

	selDB := dao.SelectRegisters(`
		and ja_respondido is false and chat_id_telegram = ?`,
		"id,suggested_answer,chat_id_mercado_livre,question,chat_id_telegram")

	for selDB.Next() {
		err = selDB.Scan(&messages.ID, &messages.SuggestedAnswer, &messages.ChatIDMercadoLivre,
			&messages.Question, &messages.ChatIDTelegram)
		shared.HandleErrors(err)
	}

	sendAI := make(map[string]interface{})
	sendAI["phrase"] = messages.Question
	sendAI["class"] = sendMsg.Msg.Text
	reqBytes, err = json.Marshal(sendAI)
	shared.HandleErrors(err)

	res, err := http.Post(shared.AiApi, "application/json", bytes.NewBuffer(reqBytes))
	shared.HandleErrors(err)

	if res.StatusCode != http.StatusOK {
		log.Println("unexpected status" + res.Status)
	}
}

func SendMessageML(sendMsg *shared.UpdateMessage) {
	var err error
	var reqBytes []byte
	var datas []interface{}
	var messages shared.MessagesInputOutput

	dao.SetTable("messages_input_output")

	datas = append(datas, sendMsg.Msg.RefChat.ID)
	dao.SetFilterValues(datas)

	selDB := dao.SelectRegisters(`
		and ja_respondido is false and chat_id_telegram = ?`,
		"id,suggested_answer,chat_id_mercado_livre,token_mercado_livre,question,chat_id_telegram")

	for selDB.Next() {
		err = selDB.Scan(&messages.ID, &messages.SuggestedAnswer, &messages.ChatIDMercadoLivre,
			&messages.TokenMercadoLivre, &messages.Question, &messages.ChatIDTelegram)
		shared.HandleErrors(err)
	}

	sendML := make(map[string]interface{})
	sendML["idpergunta"] = messages.ChatIDMercadoLivre
	sendML["text"] = sendMsg.Msg.Text
	sendML["token"] = messages.TokenMercadoLivre
	reqBytes, err = json.Marshal(sendML)
	shared.HandleErrors(err)

	res, err := http.Post(shared.MlApi, "application/json", bytes.NewBuffer(reqBytes))
	shared.HandleErrors(err)

	if res.StatusCode != http.StatusOK {
		log.Println("unexpected status" + res.Status)
	}
}

func UpdateAnswerSent(sent_answer string, field string, id int) {
	datasToUpdate := make(map[string]interface{})
	var datas []interface{}

	datasToUpdate["sent_answer"] = sent_answer
	datasToUpdate["output_data_message"] = shared.GetDateNow("timestamp")
	datasToUpdate["ja_respondido"] = true

	dao.SetTable("messages_input_output")

	datas = append(datas, id)
	dao.Update(datasToUpdate, field, datas)
}
