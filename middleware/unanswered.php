<?php

    require __DIR__.'/php-sdk/Meli/meli.php';
    require __DIR__.'/php-sdk/configApp.php';
    require_once  __DIR__.'/conexao.php';

    $conn = conn_mysql();

    $token="";

    @$meli = new Meli($appId, $secretKey);

    function getUnAnsweredQuestions(){
        global $token;
        @$meli = new Meli($appId, $secretKey);
        $params = array('access_token' => $token);
        $url = '/my/received_questions/search/';
        $result = $meli->get($url, $params);
        
        $questions = $result['body']->questions;

        for($i=0;$i<count($questions);$i++){
            if($questions[$i]->status=='UNANSWERED'){
                $u=$i;
                break;            
            }
        }
        return $questions[$u];
    }    

    function getProduto($item_id){
        $meli = new Meli($appId, $secretKey);
        $url = "/items?ids=$item_id&access_token=$token";
        $params = array();
        $result = $meli->get($url, $params);
        $nomeProduto = $result['body'][0]->body->title;
        return $nomeProduto;
    }

    if(!isset($token)){
        echo 'Não existe um token válido.';
    }else{

        $question = getUnAnsweredQuestions();

        if($question==''){
            echo "Não existe perguntas sem respostas.";
        }else{

            $nomeProduto = getProduto($question->item_id);

            $url = 'https://e8a9b93c.ngrok.io/classify';
            $ch = curl_init($url);
            $jsonData = array(
                'produto' => $nomeProduto,
                'pergunta' => $question->text,
                'id_produto' => $question->item_id,
                'id_pergunta' => $question->id,
                'token' => $token
            );

            $jsonDataEncoded = json_encode($jsonData);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            $resposta = curl_exec($ch);  

            //print_r($jsonDataEncoded);
            
        }
    }

?>