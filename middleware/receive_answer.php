<?php

    session_start();

    require './php-sdk/Meli/meli.php';
    require './php-sdk/configApp.php';


    function postReply($question_id,$text,$token){
        $meli = new Meli($appId, $secretKey);
        $params = array('access_token' => $token);
        $url = '/answers';
        $body = array('question_id' => $question_id, 'text' => $text);
        $result = $meli->post($url, $body, $params);
        return json_encode($result);	
    }

    $postdata = file_get_contents("php://input");
    if (isset($postdata)) {
        $meli = new Meli($appId, $secretKey);
        $request = json_decode($postdata);
        postReply($request->idpergunta, $request->text, $request->token);
    }

?>