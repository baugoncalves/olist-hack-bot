from utils import normalize,stemming,remove_stopwords,load_corpus

def learning(training_data):
    corpus_words = load_corpus()
    for data in training_data:
        phrase = data['phrase']
        phrase = normalize(phrase)
        phrase = remove_stopwords(phrase)
        phrase = stemming(phrase)

        class_name = data['class']
        if class_name not in list(corpus_words.keys()):
            corpus_words[class_name] = {}
        for word in phrase:
            if word not in list(corpus_words[class_name].keys()):
                corpus_words[class_name][word] = 1
            else:
                corpus_words[class_name][word] += 1
    return corpus_words


def sample():
    training_data = []
    training_data.append({"class":"Valor R$ 20.00 ", "phrase":"Qual o valor?"})
    training_data.append({"class":"Sim, temos na cor preta", "phrase":"Ainda tem na cor preta"})
    print("%s phrases included" % len(training_data))
    return training_data