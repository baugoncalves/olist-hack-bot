from flask import Flask,json,request,jsonify
from train import learning,sample
from utils import save_corpus,normalize,remove_stopwords,stemming
from answer import return_answer,include_answer
from calculator import calculate_score
from flask_cors import CORS
import requests
import json

app = Flask(__name__)
cors = CORS(app, resources={r"/train": {"origins": "*"}})
cors = CORS(app, resources={r"/classify": {"origins": "*"}})

@app.route("/", methods = ['GET'])
def health_check():
    return create_response(200,{"status":"UP"})

@app.route("/train", methods = ['GET'])
def train_with_examples():
    save_corpus(learning(sample()))
    return create_response(200,{"status":"sample phrases included"})

@app.route("/train", methods = ['POST'])
def train():
    data = request.get_json()
    phrase = data['phrase']
    class_name = data['class']
    print(phrase)
    print(class_name)
    if phrase != '':
        save_corpus(learning([{'class':class_name,'phrase':phrase}]))
    return create_response(200,{"status":"phrase included"})
    

@app.route("/classify", methods = ['POST'])
def classify():
    data = request.get_json()
    token = data['token'] 
    phrase = data['pergunta']    
    produto = data['produto']
    id_produto = data['id_produto']
    id_pergunta = data['id_pergunta']
    
    nome_produto = "Produto: {} - {}".format(produto,phrase)
    # print(data)
    respostas =  calculate_score(phrase) 
    # print(json.dumps(respostas))
    url = 'https://11f489c3.ngrok.io/receive-message-ai'
    # "chat_id": 950301668,
    data_json = json.dumps({
        "chat_id": 950301668,
        "text": nome_produto,
        "product_nome_mercado_livre": produto,
        "product_id_mercado_livre": id_produto,
        "id_mercado_livre": str(id_pergunta),
        "token_mercado_livre": str(token),
        "reply_markup": {
            "keyboard": [ respostas ]
        }
    })
    print(data_json)
    rest = requests.post(url, data = data_json)
    return create_response(200,respostas[0])

# @app.route("/chat", methods = ['GET'])
# def chat():
#     phrase = request.form.get('phrase')
#     return create_response(200,{'answer':return_answer(calculate_score(phrase)['classname'])})

@app.route("/answer", methods = ['POST'])
def save_answer():
    answer = request.form.get('answer')
    classname = request.form.get('class')
    include_answer(classname,answer)
    return create_response(200,{"status":"answer included"})

@app.route("/normalize", methods = ['GET'])
def return_normalize():
    phrase = request.form.get('phrase')
    return create_response(200,{"phrase":normalize(phrase)})

@app.route("/stopwords", methods = ['GET'])
def return_remove_stopwords():
    phrase = request.form.get('phrase')
    return create_response(200,{"phrase":remove_stopwords(normalize(phrase))})

@app.route("/stemming", methods = ['GET'])
def return_stemming():
    phrase = request.form.get('phrase')
    return create_response(200,{"phrase":stemming(remove_stopwords(normalize(phrase)))})

def create_response(statusCode, data):
    response = app.response_class(
        response=json.dumps(data),
        status=statusCode,
        mimetype='application/json'
    )
    return response

app.run(host='127.0.0.1', port=8081)

