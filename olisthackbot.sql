-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 03, 2020 at 06:11 PM
-- Server version: 10.3.22-MariaDB-0+deb10u1
-- PHP Version: 7.3.17-1+0~20200419.57+debian10~1.gbp0fda17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `olisthackbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages_input_output`
--

CREATE TABLE `messages_input_output` (
  `id` int(11) NOT NULL,
  `chat_id_telegram` bigint(20) DEFAULT NULL,
  `user_id_telegram` bigint(20) DEFAULT NULL,
  `chat_id_mercado_livre` varchar(20) DEFAULT NULL,
  `token_mercado_livre` varchar(80) DEFAULT NULL,
  `product_id_mercado_livre` varchar(30) DEFAULT NULL,
  `product_nome_mercado_livre` varchar(100) DEFAULT NULL,
  `question` varchar(120) DEFAULT NULL,
  `suggested_answer` varchar(80) DEFAULT NULL,
  `sent_answer` varchar(80) DEFAULT NULL,
  `input_data_message` timestamp NOT NULL DEFAULT current_timestamp(),
  `output_data_message` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ja_respondido` tinyint(1) DEFAULT 0,
  `lixo` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages_input_output`
--

INSERT INTO `messages_input_output` (`id`, `chat_id_telegram`, `user_id_telegram`, `chat_id_mercado_livre`, `token_mercado_livre`, `product_id_mercado_livre`, `product_nome_mercado_livre`, `question`, `suggested_answer`, `sent_answer`, `input_data_message`, `output_data_message`, `ja_respondido`, `lixo`) VALUES
(110, 950301668, 950301668, '7206301571', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'MLB1506202237', 'quero saber qual o valor do frete', 'Valor R$ 20.00 ', 'Ok thanks', '2020-05-03 20:31:06', '2020-05-03 21:06:21', 1, 0),
(111, 7094500425, 7094500425, '7206327612', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Tem na cor preta ?', 'Sim, temos na cor preta', 'Sim, temos na cor preta', '2020-05-03 20:46:05', '2020-05-03 20:49:00', 1, 0),
(112, 7094500425, 7094500425, '7206327612', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Tem na cor preta ?', 'Sim, temos na cor preta', 'Sim, temos na cor preta', '2020-05-03 20:47:05', '2020-05-03 20:50:30', 1, 0),
(113, 7094500425, 7094500425, '7206327612', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Tem na cor preta ?', 'Sim, temos na cor preta', 'Sim, temos na cor preta', '2020-05-03 20:48:05', '2020-05-03 20:50:31', 1, 0),
(114, 136927672, 136927672, '7100610100', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Tem na cor vermelha ?', 'Sim, temos na cor preta', 'Sim, temos na cor vermelho', '2020-05-03 20:51:06', '2020-05-03 20:58:25', 1, 0),
(115, 136927672, 136927672, '7100613314', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Produto: Teste De Produto  - tem branco ?', 'Sim, temos na cor preta', 'Sim, temos na cor vermelho', '2020-05-03 20:58:05', '2020-05-03 20:58:25', 1, 0),
(116, 950301668, 950301668, '7100804059', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Produto: Teste De Produto  - Hi there!! Let me to know! Is it good?', 'Yes, and it\'s a answer', 'Ok thanks', '2020-05-03 21:00:05', '2020-05-03 21:06:21', 1, 0),
(117, 950301668, 950301668, '7100807226', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Produto: Teste De Produto  - Let\'s test?', 'Sim, temos na cor preta', 'Ok thanks', '2020-05-03 21:05:05', '2020-05-03 21:06:21', 1, 0),
(118, 950301668, 950301668, '7100807226', 'APP_USR-209439576891334-050318-dce0f68b579a35bc9f0cea256fbe7853-542207204', 'MLB1506202237', 'Teste De Produto ', 'Produto: Teste De Produto  - Let\'s test?', 'Sim, temos na cor preta', 'Ok thanks', '2020-05-03 21:06:05', '2020-05-03 21:06:21', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages_input_output`
--
ALTER TABLE `messages_input_output`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages_input_output`
--
ALTER TABLE `messages_input_output`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
