package main

import (
	"log"
	"net/http"
	"olist-hack-bot/dao"
	"olist-hack-bot/routes"
	"olist-hack-bot/shared"
	"time"

	"github.com/gorilla/mux"
)

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", routes.ReceiveMessageTelegram)
	router.HandleFunc("/receive-message-ai", routes.ReceiveMessageAI)
}

func verifyIfMessageHasBeenAnswered() {
	var datas []interface{}
	var err error

	dao.SetTable("messages_input_output")

	datas = append(datas, shared.ExpiredMinutes)
	dao.SetFilterValues(datas)

	selDB := dao.SelectRegisters(`
		and ja_respondido is false and 
		TIMESTAMPDIFF(MINUTE, input_data_message, now()) > ?`,
		"id,suggested_answer,chat_id_mercado_livre,chat_id_telegram")

	for selDB.Next() {
		var messages shared.MessagesInputOutput

		err = selDB.Scan(&messages.ID, &messages.SuggestedAnswer, &messages.ChatIDMercadoLivre,
			&messages.ChatIDTelegram)
		shared.HandleErrors(err)

		// enviar para o mercado livre e para a IA

		/*routes.SendMessageAI(&shared.UpdateMessage{
			Msg: shared.Message{
				Text: messages.SuggestedAnswer,
				RefChat: shared.Chat{
					ID: messages.ChatIDTelegram,
				},
			},
		})*/
		routes.SendMessageML(&shared.UpdateMessage{
			Msg: shared.Message{
				Text: messages.SuggestedAnswer,
				RefChat: shared.Chat{
					ID: messages.ChatIDTelegram,
				},
			},
		})

		routes.UpdateAnswerSent(messages.SuggestedAnswer, "id", messages.ID)

		sendMsg := make(map[string]interface{})
		sendMsg["chat_id"] = messages.ChatIDTelegram
		sendMsg["text"] = "Devido à demora, enviamos a seguinte resposta: " + messages.SuggestedAnswer

		routes.SendMessageTelegram(shared.SendMessage{}, sendMsg)

	}

}

func startPolling1() {
	for {
		time.Sleep(1 * time.Minute)
		go verifyIfMessageHasBeenAnswered()
	}
}

func main() {
	var router *mux.Router

	go startPolling1()

	router = mux.NewRouter()
	router.Use(shared.CORS)
	setRoutes(router)

	log.Println("Server started on: http://localhost:3000")
	http.ListenAndServe(":3000", router)
}

/* https://github.com/tucnak/telebot#license
ngrok http 3000
curl -F "url=https://eccc77ae.ngrok.io"  https://api.telegram.org/bot1133187377:AAGnvCBW7hlpUkwwlY1EIyLxsqZQ5MmBFYs/setWebhook
https://github.com/go-telegram-bot-api/telegram-bot-api/blob/master/types.go*/
