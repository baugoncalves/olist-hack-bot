package shared

// https://core.telegram.org/bots/api#update
// https://core.telegram.org/bots/api#sendmessage

type UpdateMessage struct {
	Msg Message `json:"message"`
}

type Message struct {
	Text    string `json:"text"`
	RefChat Chat   `json:"chat"`
	From    User   `json:"from"`
}

type User struct {
	ID       int    `json:"id"`
	FistName string `json:"first_name"`
	UserName string `json:"username"`
	LastName string `json:"last_name"`
}

type Chat struct {
	ID int `json:"id"`
}

type SendMessage struct {
	ChatID                  int                 `json:"chat_id"`
	Text                    string              `json:"text"`
	IDMercadoLivre          string              `json:"id_mercado_livre"`
	TokenMercadoLivre       string              `json:"token_mercado_livre"`
	ProductIDMercadoLivre   string              `json:"product_id_mercado_livre"`
	ProductNomeMercadoLivre string              `json:"product_nome_mercado_livre"`
	ReplyMarkup             ReplyKeyboardMarkup `json:"reply_markup"`
}

type ReplyKeyboardMarkup struct {
	Keyboard        [][]KeyboardButton `json:"keyboard"`
	OneTimeKeyboard bool               `json:"one_time_keyboard"`
}

type KeyboardButton struct {
	Text string `json:"text"`
}

type MessagesInputOutput struct {
	ID                      int    `json:"id"`
	ChatIDTelegram          int    `json:"chat_id_telegram"`
	UserIDTelegram          int    `json:"user_id_telegram"`
	ChatIDMercadoLivre      string `json:"chat_id_mercado_livre"`
	ProductIDMercadoLivre   string `json:"product_id_mercado_livre"`
	ProductNomeMercadoLivre string `json:"product_nome_mercado_livre"`
	TokenMercadoLivre       string `json:"token_mercado_livre"`
	Question                string `json:"question"`
	SuggestedAnswer         string `json:"suggested_answer"`
}
