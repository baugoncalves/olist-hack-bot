package shared

import (
	"log"
	"time"
)

func HandleErrors(errorVar error) {
	if errorVar != nil {
		log.Fatal(errorVar.Error())
	}
}

func GetDateNow(typeDate string) string {
	dt := time.Now()
	var dateNow string

	switch typeDate {
	case "timestamp":
		dateNow = dt.Format("2006-01-02") + " " + dt.Format("15:04:05")
	case "date":
		dateNow = dt.Format("2006-01-02")
	case "hour":
		dateNow = dt.Format("15:04:05")
	}

	return dateNow
}
