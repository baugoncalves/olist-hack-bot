package dao

import (
	"database/sql"
	"slapfestas-site-api/messages"
)

var currentTable string
var primaryKey string
var filterValues []interface{}
var databaseCustomer string
var lastIdSaved int64

func SetDatabase(db string) {
	databaseCustomer = "slapfestas"

	if len(db) > 0 {
		databaseCustomer = db
	}
}

func SetTable(table string) {
	var porta = "3306"

	SetDatabase("olisthackbot")
	currentTable = table
	connDB = SetConnectionDB(databaseCustomer, porta)
}

func SetFilterValues(values []interface{}) { //metodo para setar os dados que se quer trabalhar
	filterValues = values
}

func setNilFilterValues() {
	filterValues = nil
}

func SelectRegisters(condition, fields string) *sql.Rows {
	var sqlAux string
	var registers *sql.Rows
	var err error

	sqlAux = "select *from " + currentTable + " where lixo=0"

	switch true {
	case len(fields) > 0 && len(condition) > 0:
		sqlAux = "select " + fields + " from " + currentTable + " where lixo=0 " + condition
	case len(condition) > 0:
		sqlAux = "select *from " + currentTable + " where lixo=0 " + condition
	case len(fields) > 0:
		sqlAux = "select " + fields + " from " + currentTable + " where lixo=0"
	}

	if len(filterValues) > 0 {
		registers, err = connDB.Query(sqlAux, filterValues...)
	} else {
		registers, err = connDB.Query(sqlAux)
	}
	messages.HandleErrors(err)

	setNilFilterValues()

	CloseDB()

	return registers
}

func GetFetchById(id int, fields string) *sql.Row {
	var sqlAux string
	var registers *sql.Row

	setPrimaryKey()

	sqlAux = "select *from " + currentTable + " where " + primaryKey + " = ?"

	if len(fields) > 0 {
		sqlAux = "select " + fields + " from " + currentTable + " where " + primaryKey + " = ?"
	}

	registers = connDB.QueryRow(sqlAux, id)

	CloseDB()

	return registers

}

func GetFetchByUrlAmigavel(urlamigavel string, fields string) *sql.Row {
	var sqlAux string
	var registers *sql.Row

	setPrimaryKey()

	sqlAux = "select *from " + currentTable + " where urlamigavel = ?"

	if len(fields) > 0 {
		sqlAux = "select " + fields + " from " + currentTable + " where urlamigavel = ?"
	}

	registers = connDB.QueryRow(sqlAux, urlamigavel)

	CloseDB()

	return registers

}

func CustomQuery(sqlS string) *sql.Rows {
	var registers *sql.Rows
	var err error

	registers, err = connDB.Query(sqlS, filterValues...)
	messages.HandleErrors(err)

	defer CloseDB()
	setNilFilterValues()

	return registers
}

func setPrimaryKey() {
	var sqlAux string
	var datas []interface{}
	var registers *sql.Row
	var err error

	sqlAux = `SELECT key_column_usage.column_name
		FROM   information_schema.key_column_usage
		WHERE  table_schema = schema()
		AND    constraint_name = ?
		AND    table_name = ?`

	datas = append(datas, "PRIMARY")
	datas = append(datas, currentTable)

	registers = connDB.QueryRow(sqlAux, datas...)
	err = registers.Scan(&primaryKey)
	messages.HandleErrors(err)
}

func Save(datas map[string]interface{}) {
	var sqlAux string
	var err error

	sqlAux = "insert into " + currentTable + " ("
	sqlAux += destructuring("field", datas)
	sqlAux += ") values ("
	sqlAux += destructuring("value", datas)
	sqlAux += ")"

	action, err := connDB.Prepare(sqlAux)
	messages.HandleErrors(err)
	res, err := action.Exec(filterValues...)
	messages.HandleErrors(err)

	lastIdSaved, err = res.LastInsertId()
	messages.HandleErrors(err)

	setNilFilterValues()
	CloseDB()
}

func Update(datas map[string]interface{}, field string, values []interface{}) {
	var sqlAux, auxValues string
	// var err error

	sqlAux = "update " + currentTable + " set "
	sqlAux += destructuring("update", datas)

	for _, val := range values {
		auxValues += ",?"
		filterValues = append(filterValues, val)
	}
	auxValues = auxValues[1:]

	sqlAux += " where " + field + " in(" + auxValues + ")"

	action, err := connDB.Prepare(sqlAux)
	messages.HandleErrors(err)
	_, err = action.Exec(filterValues...)
	messages.HandleErrors(err)

	setNilFilterValues()
	CloseDB()
}

func destructuring(tipo string, datas map[string]interface{}) string {
	var fields string

	switch tipo {
	case "field":
		for key, value := range datas {
			fields += "," + key
			filterValues = append(filterValues, value)
		}
	case "value":
		for range datas {
			fields += ",?"
		}
	case "update":
		for key, value := range datas {
			fields += "," + key + "=?"
			filterValues = append(filterValues, value)
		}
	}

	return fields[1:]
}

func GetLastID() int {
	return int(lastIdSaved)
}
