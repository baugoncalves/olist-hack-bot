package dao

import (
	"database/sql"
	"slapfestas-site-api/messages"

	_ "github.com/go-sql-driver/mysql"
)

var connDB *sql.DB

func SetConnectionDB(dbname, porta string) *sql.DB {
	var openError error
	var driver, user, password, database, server, port string

	driver = "mysql"
	user = "root"
	password = "123"
	database = dbname
	server = "127.0.0.1"
	port = porta

	connDB, openError = sql.Open(driver, user+":"+password+"@tcp("+server+":"+port+")/"+database)

	messages.HandleErrors(openError)

	openError = connDB.Ping()
	messages.HandleErrors(openError)

	return connDB
}

func CloseDB() {
	connDB.Close()
}
